<?php
// =================================CONECTAR A BD
function conectarDB($schema = 'despliegue', $user = 'root', $pwd = 'despliegue', $host = 'localhost') {
	try {
		$dsn = "mysql:host=$host;dbname=$schema";
		$db = new PDO ( $dsn, $user, $pwd );
		return $db;
	} catch ( PDOException $e ) {
		print ('ERROR de conexión') ;
		die ();
	}
}
// =================================CONECTAR A BD


// =================================INSETAR PRODUCTOS
function insertar($productos) {
	$db = conectarDB ();
	$consulta = "insert into productos(nombre,precio) values (:nombre,:precio)";
	$resultado = $db->prepare ( $consulta );
	
	$resultado->execute ( [ 
			':nombre' => $productos [0],
			':precio' => $productos [1] 
	] );
}
// =================================INSETAR PRODUCTOS


// =================================PINTA TABLA PRODUCTOS INTRODUCIDOS
function consultar() {
	$db = conectarDB ();
	$consulta = <<<SQL
	select * from productos
SQL;
	$resultado = $db->query ( $consulta );
	if (! $resultado) {
		
		print ('ERROR al ejecutar query') ;
	} else {
		echo "<table border=\"1\">";
		echo "<tr><td>PRODUCTO</td><td>PRECIO</td></tr>";
		foreach ( $resultado as $fila ) {
			echo "<tr><td>{$fila['nombre']}</td><td>{$fila['precio']}</td></tr>";
		}
		echo "</table>";
	}
}
// =================================PINTA TABLA PRODUCTOS INTRODUCIDOS

?>
<html>
<head>
<title>Inserta Productos</title>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
</head>
<style>

body{
font-family:Agency FB;
background-color:silver;
margin:auto;
}

table{
background-color:white;    
}

#resultado{
border:1px solid gray;
padding:10px 0px 10px 55px;
}

</style>
<body>
	<fieldset>
		<legend>Indroduzca los datos</legend>
		<form action="productos.php" method="get">
			PRODUCTO: <br/><input type="text" name="producto"> <br /> 
			PRECIO: <br/><input type="text" name="precio"> <hr />
			<input type="submit" name="insertar" value="Insertar">
			<br />
		</form>
	</fieldset>
	<br/>

<div id="resultado">
<?php
consultar ();

// =================================CONSULTAR DATOS INTRODUCIDOS

$producto = (isset ( $_REQUEST ['producto'] )) ? $_REQUEST ['producto'] : null;
$precio = (isset ( $_REQUEST ['precio'] )) ? $_REQUEST ['precio'] : null;

$precio = ( int ) $precio;

if (isset ( $_REQUEST ['producto'] ) && isset ( $_REQUEST ['precio'] )) {
	
	if ($producto != null && $precio != null) {
		$datos = [ 
				$producto,
				$precio 
		];
		insertar ( $datos );
		echo "Datos introducidos correctamente";
	} else {
		
		echo "Debes introducir un producto y su precio.";
	}
}

?>
</div>
</body>
</html>