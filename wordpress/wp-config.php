<?php

/** 

 * Configuración básica de WordPress.

 *

 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,

 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,

 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing

 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.

 *

 * This file is used by the wp-config.php creation script during the

 * installation. You don't have to use the web site, you can just copy this file

 * to "wp-config.php" and fill in the values.

 *

 * @package WordPress

 */


// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //

/** El nombre de tu base de datos de WordPress */

define('DB_NAME', 'wordpress');


/** Tu nombre de usuario de MySQL */

define('DB_USER', 'wordpress');


/** Tu contraseña de MySQL */

define('DB_PASSWORD', 'wordpress');


/** Host de MySQL (es muy probable que no necesites cambiarlo) */

define('DB_HOST', 'localhost');


/** Codificación de caracteres para la base de datos. */

define('DB_CHARSET', 'utf8mb4');


/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */

define('DB_COLLATE', '');


/**#@+

 * Claves únicas de autentificación.

 *

 * Define cada clave secreta con una frase aleatoria distinta.

 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}

 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.

 *

 * @since 2.6.0

 */

define('AUTH_KEY', 'gg]!oSE*=*kIFb lbz-yEg$rl&)wM+FoSp-IM/@zmge4;3F1+-GX,?[*dV<ZzBaK');

define('SECURE_AUTH_KEY', 'pl^jClGeD?PSZ|:p-yS>eGi2xaKw@9gTPFxHe$T[_,y6GoUB24^e/A&e~X|au3o-');

define('LOGGED_IN_KEY', 'bG:|e{iE-d()&H?~,{QDKd;l9t)5og6[!|6|yYy.>NnB{u.b2Qt,M@,H!v|WTN(e');

define('NONCE_KEY', 'M(_rrGCF1V.Pu76%,*lF{Zeag%AjaU]@S0uU8!;#M[xK^Se3P(rbf{7a}FS<Zf6.');

define('AUTH_SALT', 'iHXSE=`- t}d7Z|xdiKr.[C,W<Delx@?Wvo#hBM*k_7_O=!w@(4#qBNk)-w~K DB');

define('SECURE_AUTH_SALT', ',2=e(3mI?z*/3J|RpeE c_Q9:SO`1MpF+7u=|!T 9W}pffA<?hood5VtgCf1IAda');

define('LOGGED_IN_SALT', ']63tiJ|&{!B?h7;LZt}%Y&Y/g*48,[jQqz+_+pk] xt ^-C9q` 5y*x3-$xR27}.');

define('NONCE_SALT', '$|B<`>%p`B;gRE&,D7+C|G,3w=I|416B~+ia>0{D[R6gSl/dOfE9{8[x^5< L1z8');


/**#@-*/


/**

 * Prefijo de la base de datos de WordPress.

 *

 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.

 * Emplea solo números, letras y guión bajo.

 */

$table_prefix  = 'wp_';



/**

 * Para desarrolladores: modo debug de WordPress.

 *

 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.

 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG

 * en sus entornos de desarrollo.

 */

define('WP_DEBUG', false);


/* ¡Eso es todo, deja de editar! Feliz blogging */


/** WordPress absolute path to the Wordpress directory. */

if ( !defined('ABSPATH') )

	define('ABSPATH', dirname(__FILE__) . '/');


/** Sets up WordPress vars and included files. */

require_once(ABSPATH . 'wp-settings.php');


